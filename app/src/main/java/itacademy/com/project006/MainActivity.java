package itacademy.com.project006;

import android.content.SharedPreferences;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etFirstName, etLastName, etId;
    private Button btnSave, btnGet, btnClear, btnUpdate;
    private TextView tvResult;

    private SQLiteHelper sqLiteHelper;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        sqLiteHelper = new SQLiteHelper(this);

        tvResult = findViewById(R.id.tvResult);

        etFirstName = findViewById(R.id.etFirstName);
        etLastName = findViewById(R.id.etLastName);
        etId = findViewById(R.id.etId);

        btnSave = findViewById(R.id.btnSave);
        btnGet = findViewById(R.id.btnGet);
        btnClear = findViewById(R.id.btnClear);
        btnUpdate = findViewById(R.id.btnUpdate);

        btnSave.setOnClickListener(this);
        btnGet.setOnClickListener(this);
        btnClear.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        String firstName;
        String lastName;
        switch (view.getId()) {
            case R.id.btnSave:
                firstName = etFirstName.getText().toString();
                lastName = etLastName.getText().toString();
                if (!firstName.isEmpty() && !lastName.isEmpty()) {
                    sqLiteHelper.saveData(firstName, lastName);
//                    SharedPreferences.Editor editor = preferences.edit();
//                    editor.putString("firstname", firstName);
//                    editor.putString("lastname", lastName);
//                    editor.apply();

                } else {
                    Toast.makeText(this, "Data is empty", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnGet:
                String result = sqLiteHelper.getData();
                tvResult.setText(result);

//                firstName = preferences.getString("firstname", "");
//                lastName = preferences.getString("lastname", "");
//                String result = firstName + " " + lastName;
                tvResult.setText(result);
                break;
            case R.id.btnClear:
                sqLiteHelper.clearData();
                break;
            case R.id.btnUpdate:
                firstName = etFirstName.getText().toString();
                lastName = etLastName.getText().toString();
                String id = etId.getText().toString();

                sqLiteHelper.updateData(id, firstName, lastName);

                break;
        }
    }
}
