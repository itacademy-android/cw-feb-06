package itacademy.com.project006;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Isakov on 03-Feb-18.
 */

public class SQLiteHelper extends SQLiteOpenHelper {

    private final static String DB_NAME = "ACADEMY";
    private final static int DB_VERSION = 2;

    private final static String TABLE_NAME = "USER";
    private final static String ID = "_id";
    private final static String FIRST_NAME = "FIRST_NAME";
    private final static String LAST_NAME = "LAST_NAME";
    private final static String PHONE_NUMBER = "PHONE_NUMBER";
    private final static String EMAIL = "EMAIL";
    private final static String AGE = "AGE";

    private final static String CREATE_TABLE_USER = "CREATE TABLE IF NOT EXISTS " +
            TABLE_NAME + "(" +
            ID + " INTEGER_PRIMARY_KEY, " +
            FIRST_NAME + " TEXT, " +
            LAST_NAME + " TEXT, " +
            PHONE_NUMBER + " TEXT, " +
            EMAIL + " TEXT, " +
            AGE + " INTEGER" +
            ");";

    public SQLiteHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_USER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public void saveUser(UserModel userModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(FIRST_NAME, userModel.getFirstName());
        cv.put(LAST_NAME, userModel.getLastName());
        cv.put(PHONE_NUMBER, userModel.getPhoneNumber());
        cv.put(EMAIL, userModel.getEmail());
        cv.put(AGE, userModel.getAge());

        long rowID = db.insert(TABLE_NAME, null, cv);
        Log.d("Row inserted: ", "ID = " + rowID);
        db.close();
    }

    public void saveData(String firstName, String lastName) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(FIRST_NAME, firstName);
        cv.put(LAST_NAME, lastName);

        long rowID = db.insert(TABLE_NAME, null, cv);
        Log.d("Row inserted: ", "ID = " + rowID);
        db.close();
    }

    public String getData() {
        String result = "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null);

        if (cursor.moveToFirst()) {
            int firstNameIndex = cursor.getColumnIndex(FIRST_NAME);
            int lastNameIndex = cursor.getColumnIndex(LAST_NAME);
            do {
                String firstName = cursor.getString(firstNameIndex);
                String lastName = cursor.getString(lastNameIndex);
                result = firstName + " " + lastName;
            } while (cursor.moveToNext());
            Log.d("Data received", "amount: " + cursor.getCount());
        } else {
            Log.d("Data is empty", "amount: " + cursor.getCount());
        }
        cursor.close();
        db.close();

        Log.d("CURSOR_STATE", cursor.isClosed() +"");
        return result;
    }

    public void updateData(String id, String firstName, String lastName) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(FIRST_NAME, firstName);
        cv.put(LAST_NAME, lastName);

        long rowID = db.update(TABLE_NAME, cv, "_id = ?", new String[] {id});
        Log.d("LOG_TAG_NAMES", "row updated, ID = " + rowID);
        db.close();
    }

    public void clearData() {
        SQLiteDatabase db = this.getWritableDatabase();
        long clearCount = db.delete(TABLE_NAME, null, null);
        Log.d("DATA DELETED", "amount: " + clearCount);
        db.close();
    }
}
