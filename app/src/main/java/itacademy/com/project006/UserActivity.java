package itacademy.com.project006;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class UserActivity extends AppCompatActivity implements View.OnClickListener{
    private Button btnSave, btnGet, btnClear;
    private EditText etFirstName, etLastName, etPhoneNumber, etEmail, etAge;
    private TextView tvResult;

    private SQLiteHelper sqLiteHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        sqLiteHelper = new SQLiteHelper(this);

        etFirstName = findViewById(R.id.etFirstName);
        etLastName = findViewById(R.id.etLastName);
        etPhoneNumber = findViewById(R.id.etPhoneNumber);
        etEmail = findViewById(R.id.etEmail);
        etAge = findViewById(R.id.etAge);

        btnSave = findViewById(R.id.btnSave);
        btnGet = findViewById(R.id.btnGet);
        btnClear = findViewById(R.id.btnClear);

        tvResult = findViewById(R.id.tvResult);

        UserModel userModel = new UserModel();
        userModel.setFirstName("John");
        userModel.setLastName("Smith");
        userModel.setPhoneNumber("+19995959562");
        userModel.setEmail("johnsmith@gmail.com");
        userModel.setAge(35);

        btnSave.setOnClickListener(this);
        btnGet.setOnClickListener(this);
        btnClear.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        UserModel userModel = new UserModel();
        switch (view.getId()) {
            case R.id.btnSave:
                String firstName = etFirstName.getText().toString();
                userModel.setFirstName(firstName);

                String lastName = etLastName.getText().toString();
                userModel.setLastName(lastName);

                String phoneNumber = etPhoneNumber.getText().toString();
                userModel.setPhoneNumber(phoneNumber);

                String email = etEmail.getText().toString();
                userModel.setEmail(email);

                String age = etAge.getText().toString();
                userModel.setAge(Integer.parseInt(age));

                sqLiteHelper.saveUser(userModel);
                break;
            case R.id.btnGet:
//                UserModel model = sqLiteHelper.getUser();
                break;
            case R.id.btnClear:

                break;
        }
    }
}
